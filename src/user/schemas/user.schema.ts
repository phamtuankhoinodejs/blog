import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Field, ID, InputType, ObjectType } from '@nestjs/graphql';

export type UserDocument = User & Document;

@Schema()
@ObjectType()
export class User {
  @Field(() => ID) //<- GraphQL
  _id: string; //<- TypeScript

  @Prop({ required: true, unique: true }) //<- Mongoose
  @Field()
  username: string;

  @Prop({ required: true}) //<- Mongoose
  password: string;

  @Prop({ required: true })
  @Field()
  name: string;

  @Prop({ required: true })
  @Field()
  email: string;

  @Prop({ required: true })
  @Field()
  image: string;
}

export const UserSchema = SchemaFactory.createForClass(User);


