import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User, UserDocument } from './schemas/user.schema';
import * as bcrypt from 'bcrypt'




@Injectable()
export class UserService {
  constructor(@InjectModel(User.name) private UserModel: Model<UserDocument>) {}

  async fillAllUser(){
    return this.UserModel.find()
  }

  async findOne(username){
    return this.UserModel.findOne({username: username});
  }
  async createAccount(user){
    return this.UserModel.create(user);
  }

  async update(user){
    const password = await bcrypt.hash(user.password, 10);
    const UserUpdate = await this.UserModel.findByIdAndUpdate({_id: user._id}, {
      username: user.username, 
      password: password,
      name: user.name, 
      email: user.email,
      image: user.image
    }, {new: true});
    // console.log(password);
    return UserUpdate;
  }
}

