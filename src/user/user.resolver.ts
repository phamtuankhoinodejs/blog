import { User } from './schemas/user.schema';
import { UserService } from './user.service';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth-guard';
import { CreateUserInput } from './dto/create-user.dto';
import { UpdateUserInput } from './dto/update-user.dto';
import { GraphQLUpload, FileUpload } from 'graphql-upload';
import { createWriteStream } from 'fs';



@Resolver(() => User)
export class UserResolver {
    constructor(
        private userService: UserService
    ){}


    // getalluser
    @Query(() => [User])
    @UseGuards(JwtAuthGuard)
    async getalluser() {
      return this.userService.fillAllUser();
    }

  

    //update user
    @Mutation(() => User)
    async update(@Args('updateUser') user: UpdateUserInput){
      return this.userService.update(user);
    }

    //upload file
    @Mutation(() => Boolean)
    async uploadFile(@Args({name: 'file', type: () => GraphQLUpload})
    {
        createReadStream,
        filename
    }: FileUpload): Promise<boolean> {
        return new Promise(async (resolve, reject) => 
            createReadStream()
                .pipe(createWriteStream(`./uploads/${filename}`))
                .on('finish', () => resolve(true))
                .on('error', () => reject(false))
        );
    }

}
