import { Field, InputType } from "@nestjs/graphql";

export class CreateUserDto {}
@InputType()
export class CreateUserInput {
  @Field()
  username: string;

  @Field()
  password: string;

  @Field()
  name: string;

  @Field()
  email: string;

  @Field()
  image: string;
}

