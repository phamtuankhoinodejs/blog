import { Field, ObjectType } from "@nestjs/graphql";
import { User } from "src/user/schemas/user.schema";


@ObjectType()
export class ReponseLogin{
    @Field()
    access_token: string;

    @Field(() => User)
    user: User;
}
