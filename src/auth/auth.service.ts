import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/user/schemas/user.schema';
import { UserService } from 'src/user/user.service';
import * as bcrypt from 'bcrypt'


@Injectable()
export class AuthService {
    constructor(
        private userService: UserService,
        private jwtService: JwtService
      ) {}
    
      async validateUser(username: string, pass: string): Promise<any> {
        const user = await this.userService.findOne(username);

        const passvld = bcrypt.compare(pass, user.password); //password compare
        // console.log(passvld);

        if (user && passvld) {
          // const { password, ...result } = user;
          return user;
        }
        return null;
      }
    
      async login(user: User) {

        const signJwt = {username: user.username, sub: user._id}

        return {
          access_token: this.jwtService.sign(signJwt),
          user
        };
      }

      async signin(user){
        const password = await bcrypt.hash(user.password, 10);
        user.password = password;
        return this.userService.createAccount(user);
      }
}
